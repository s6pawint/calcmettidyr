# Automatically read the data from the input folder
readCalcMetData <- function(filePath="./inputdata"){
  # get the list of all the TXT file in the directory 
  ghgFileList <- list.files(filePath, pattern = "*.TXT")
  # Initialize the list of data frames, loaded from the input folder
  dataframesList <<- list()
  # loop through all the listed TXT files
  for(i in ghgFileList){
    # assign the data frames and read current the ghg table from the directory. 
    dataframesList[[paste0("ghg_",substring(i,1,8))]] <<- read.delim(paste0(filePath, "/", i), sep = "\t", fileEncoding = "ISO-8859-1")
    # Add a column with Time and Date to each data frame
    DateTime <- as.POSIXct(paste(dataframesList[[paste0("ghg_",substring(i,1,8))]][, "Date"], dataframesList[[paste0("ghg_",substring(i,1,8))]][, "Time"]),format = "%Y-%m-%d %H:%M:%S")
    dataframesList[[paste0("ghg_",substring(i,1,8))]][, "DateTime"] <<- DateTime
  }
  
  # Get all the dates from the files names and format them
  allDates <- substring(ghgFileList, 1, (nchar(ghgFileList)-4))
  allDates <- as.Date(allDates, format="%d.%m.%y")
  # Return the Dates
  return(allDates)
}

# Reduce the measurements to the ones during the observation intervals from a timetable
filterTimes <- function(ghgfile, timetable){
  # Initialize the Output data frame
  exportframe <- data.frame(Watervapor=NA, Unit=NA, CO2=NA, Unit=NA, CH4=NA, Unit=NA, N2O=NA, Unit=NA, NH3=NA, Unit=NA, AmbientPressure=NA, Unit=NA, KPA=NA, Plot=NA)
  
  # Loop through the row indices of the recorded start times at a certain date
  for(j in 1:length(timetable$startDT)){
    # Loop through the row indices of measurements at a certain date
    for(i in 1:length(ghgfile$DateTime)){
      # check if the measured time is in range of the recorded start and end time
      if(ghgfile$DateTime[i] >= timetable$startDT[j] & ghgfile$DateTime[i] <= timetable$endDT[j]){
        # if yes, collect the relevant measurements, indicated by their column indices
        exportline <- c(ghgfile[i, indices], ghgfile[i,26]/10, timetable$Plot[j])
        # add the collected data to the output data frame
        exportframe[i,] <- exportline
      }
    }
  }
  # Remove NA's and return the output data frame
  exportframe <- na.omit(exportframe)
  return(exportframe)
}

# assign the observation times from to a single data frame from the readCalcMetData-function
assignMeasurementTimes <- function(filterTimesOutput, measurementTimes){
  # Set the initial index of the iteration
  lastIndex=0
  # loop through row indices -1 of the output of the filterTimeOutput function
  for(i in 1:(length(filterTimesOutput$Plot)-1)){
    # Check if the current Plot and the Plot of the next row are different
    if(filterTimesOutput$Plot[i] != filterTimesOutput$Plot[i+1]){
      # if yes, assign the measurement time, and store value of this index
      filterTimesOutput$Time[i] <- measurementTimes[i-lastIndex]
      lastIndex=i
    }
    else{
      # if no, just assign the measurement time
      filterTimesOutput$Time[i] <- measurementTimes[i-lastIndex]
    }
  }
  
  # return the data frame with the added measurement time
  return(filterTimesOutput)
}

# Analyze the linear regression of the concentration of the measured ghg and the time. 
# This functions gives the user also the ability to manually declare the indices that should be removed
analyzeSubsets <- function(assignMeasurementTimesOutput, gasSwitch=NA, currentDate="2023-05-22"){
  # Get the string of the ghg-gas variant
  gasColString <- gasIndexer(gasSwitch)
  
  # Initialize a list for indices that are to be removed, as a GLOBAL variable
  indicesForRemoval <<- list()
  
  # Initialize the data frame of the Output
  outputFrame <- data.frame("r2"=NA, "slope"=NA, "slopePval"=NA, "Plot"=NA, "Date"=NA)
  # Rename the colums so that they are unique to the ghg-gas variant
  colnames(outputFrame)[1:3] <- c(paste0("r2",gasColString), paste0("slope", gasColString), paste0("slopePval", gasColString))
  # Remove NA's
  outputFrame <- na.omit(outputFrame)
  
  # Get the unique plots of the current measurement day
  plots <- unique(assignMeasurementTimesOutput$Plot)
  # loop through the plots
  for(i in plots){
    # Create a subset, filtered by the current plot
    currentSubset <- subset(assignMeasurementTimesOutput, assignMeasurementTimesOutput$Plot==i)
    # Get the mean of the ambient air pressure measured on this plot
    currentSubKPA <- mean(currentSubset$AmbientPressure)
    # Get the sample size of the current plot
    currentN <- length(currentSubset[, gasColString])
    
    # Extract the current stats (R^2, slope, P-value of Slope)
    currentStats <- extractStats(currentSubset$Time, currentSubset[, gasColString]) 
    # get the indices of the points. Use the match function to make sure that the indices and the measurements align
    indexLabels <- match(currentSubset[, gasColString], currentSubset[, gasColString])
    # Get the space above the measurement points in the plot for the labels
    indexSpaces <- pointLabelSpace(currentSubset[, gasColString], 0.1)
    
    # Plot the concentration of the ghg-gas variant against the time
    plot(x=currentSubset$Time, y=currentSubset[, gasColString], 
         main=paste(gasColString, ", ", currentDate, ", Plot ", i), 
         xlab="Time [h]", ylab=paste(gasColString, "[ppm]"), 
         ylim=c(min(currentSubset[, gasColString]), max(indexSpaces)))
    # add the index of each point as label
    text(currentSubset$Time, indexSpaces, labels = indexLabels)
    
    # Print the summary of the linear regression of the ghg concentration against time
    print(summary(lm(currentSubset$Time~currentSubset$CO2)))
    
    # Give the user the possibility to enter the indices of points that should be removes, beased upon the plot and the summary
    removeIndicesVector <- userIO(currentN)
    # Fill the list with these indices, again as GLOBAL variable
    indicesForRemoval[[currentDate]][[paste0("Plot_",i)]] <<- removeIndicesVector
    
    #readline(prompt="Press [enter] to continue")
    outputFrame[i,] <- c(currentStats, i, currentDate)
  }
  
  return(na.omit(outputFrame))
}